package com.begear.javaripasso.eserciziooggettimetodi.util;

import java.util.Scanner;

public class ScannerManager {

	public static int inserisciIntero() {

		Scanner scanner = new Scanner(System.in);

		int intero = 0;
		System.out.println("Inserisci intero");

		if (scanner.hasNextInt()) {

			intero = scanner.nextInt();
			return intero;
		}

		String flush = scanner.nextLine();
		return inserisciIntero();

	}

	// ricorsione

	public static double inserisciDouble() {

		Scanner scanner = new Scanner(System.in);
		double d = 0;
		System.out.println("Inserisci double");

		if (scanner.hasNextDouble()) {

			d = scanner.nextDouble();
			return d;
		}

		String flush = scanner.nextLine();
		return inserisciDouble();

	}

	public static String inscrisciStringa() {
		Scanner scanner = new Scanner(System.in);
		String string;

		System.out.println("Inserisci una stringa");

		if (scanner.hasNextLine()) {
			string = scanner.nextLine();
			return string;
		}
		String flush = scanner.nextLine();
		return inscrisciStringa();
	}

}
