package com.begear.javaripasso.eserciziooggettimetodi.controller;

public class EserciziCicli {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// while
		int a = 0;
		int b = 0;
		while (a <= 20 && b <= 15) {
			System.out.println("a: " + a);
			System.out.println("b: " + b);
			a++;
			b++;
		}

		// switch-case
		char lettera = 'a';
		switch (lettera) {
		case 'a':
			System.out.println("lettera inserita: " + lettera);
			break;
		case 'b':
			System.out.println("lettera inserita: " + lettera);
			break;
		case 'c':
			System.out.println("lettera inserita: " + lettera);
			break;
		default:
			System.out.println("lettera insertia non rientra nel case: " + lettera);
			break;
		}

		// do while
		int variabile = 10;

		do {
			variabile++;
			System.out.println("do while: " + variabile);
		} while (variabile < 10);

	}

}
