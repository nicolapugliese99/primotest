package com.begear.javaripasso.eserciziooggettimetodi.controller;

import java.sql.Date;

import com.begear.javaripasso.eserciziooggettimetodi.modal.Auto;

public class main {
	public static void main(String[] args) {
		Auto auto = new Auto("Alfa Romeo", "Mito", 2000, 1112.2D);
		System.out.println(auto.toString());
		auto.corri();
		System.out.println(auto.getVel());
		auto.frena();
		System.out.println(auto.getVel());
		auto.frena();
		auto.frena();
		auto.dec(auto.getVel());
		System.out.println(auto.getVel());
		auto.corri();
		auto.frena();
	}
}
