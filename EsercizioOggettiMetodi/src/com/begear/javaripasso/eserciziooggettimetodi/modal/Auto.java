package com.begear.javaripasso.eserciziooggettimetodi.modal;

import java.sql.Date;

public class Auto {
	private String marca;
	private String modello;
	private int cilindrata;
	private double prezzo;
	private int vel;

	public Auto(String marca, String modello, int cilindrata, double prezzo) {
		super();
		this.marca = marca;
		this.modello = modello;
		this.cilindrata = cilindrata;
		this.prezzo = prezzo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModello() {
		return modello;
	}

	public void setModello(String modello) {
		this.modello = modello;
	}

	public int getCilindrata() {
		return cilindrata;
	}

	public void setCilindrata(int cilindrata) {
		this.cilindrata = cilindrata;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	@Override
	public String toString() {
		return "Auto [marca=" + marca + " , modello=" + modello + ", cilindrata=" + cilindrata + ",prezzo=" + prezzo
				+ "]";
	}

	public int getVel() {
		return vel;
	}

	public void setVel(int vel) {
		this.vel = vel;
	}

	public int corri() {
		int velattuale = this.vel;
		// variabile di metodo
		int ac = 10;
		velattuale = this.vel + 10;
		this.setVel(velattuale);

		return velattuale;
	}

	public void frena() {
		if (this.vel != 0) {
			System.out.println("FRENA");
		} else {
			System.out.println("macchina gi� ferma");
		}
	}

	public void dec(int i) {
		int dec = 5;

		if (getVel() > 0) {
			int velAtt = this.vel - dec;
			this.setVel(velAtt);
		} else {
			System.out.println("Errore macchina gi� ferma");
		}

	}
}